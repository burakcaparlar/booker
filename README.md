# README #

    This README would normally document whatever steps are necessary to get your application up and running.

# Booker #

    A python client library for HotelsPro

    Version 1.0.0

    Learn : https://bitbucket.org/burakcaparlar/booker/

# Installation #

    $ (sudo) pip install Booker

# Usage #

    from booker import Booker

## Example : search() ##

    from booker import Booker

    params = {'checkin': '2016-11-20', 'checkout': '2016-11-22', 'pax': '1',
              'destination_code': '206ec', 'client_nationality': 'tr',
              'currency': 'USD', 'name': '1,test,test,adult'}


    book = Booker(password="password", username="username")

    book.search(params=params)
    book.availability(product_code="product_code")
    book.provision(product_code="product_code")
    book.book(book_code="book_code", params=params)
    book.bookings()  # or book.bookings("book_code")

### Response ###
    {
    u'client_nationality': u'tr',
    u'status': u'succeeded',
    u'hotel_payment_info': [
        {
        u'hotel_currency': None,
        u'hotel_price': None
        }
    ],
    u'code': u'BWEQNCK9CTQL',
    u'destination_code': u'206ec',
    u'nonrefundable': False,
    u'provider_args': [

    ],
    u'minimum_selling_price': None,
    u'created_at': u'2016-09-28 11:39:24.925080+00:00',
    u'hotel_code': u'135f3a',
    u'currency': u'GBP',
    u'confirmation_numbers': [
        {
        u'confirmation_number': u'164-2867477',
        u'agency_ref_id': u'N/A',
        u'cost': u'150.85',
        u'rooms': [
            {
            u'room_description': u'TWIN WITH SHARED BATHROOM',
            u'room_type': u'TWN-U02'
            }
        ],
        u'provider': u'hbeds',
        u'names': [
            u'Burak Caparlar'
        ]
        }
    ],
    u'pay_at_hotel': False,
    u'mealtype_code': u'BC',
    u'special_request': None,
    u'policies': [
        {
        u'ratio': u'0.33',
        u'days_remaining': 4
        }
    ],
    u'price': u'150.85',
    u'checkout': u'2016-10-03',
    u'checkin': u'2016-09-30',
    u'view': False
    }