from setuptools import find_packages, setup

setup(
    name='booker',
    version='1.0.0',
    packages=find_packages(),
    url='https://bitbucket.org/burakcaparlar/booker',
    license='GPL',
    author='Burak Caparlar',
    author_email='burak.caparlar@gmail.com',
    description='Hotelspro.com api client',
    install_requires=['requests'],
    classifiers=(
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
    ),
)
