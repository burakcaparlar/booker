from booker import Booker

if __name__ == '__main__':
    # params for Booker Class __init__
    params = {'checkin': '2016-11-20', 'checkout': '2016-11-22', 'pax': '1',
              'destination_code': '206ec', 'client_nationality': 'tr',
              'currency': 'USD', 'name': '1,test,test,adult'}

    product_code = "product_code"
    book_code = "book_code"

    book = Booker(password="password", username="username")

    book.search(params=params)
    book.availability(product_code=product_code)
    book.provision(product_code=product_code)
    book.book(book_code=book_code, params=params)
    book.bookings()  # or book.bookings(book_code)
