import requests


class Booker(object):
    """
    Requests is an user information with HTTP and JSON library
    You can search according to the criteria with start
    And You can perform the cancellation by cancel

    :copyright: (c) 2016 by Burak Caparlar.
    :license: Apache 2.0, see LICENSE for more details.

    """

    def __init__(self, password, username):
        self.BASE_URL = "http://localhost:8000/api/v2"
        self.SEARCH_URL = self.BASE_URL + "/search"
        self.AVAILABILITY_URL = self.BASE_URL + "/availability/%s"
        self.PROVISION_URL = self.BASE_URL + "/provision/%s"
        self.BOOK_URL = self.BASE_URL + "/book/%s"
        self.CANCEL_URL = self.BASE_URL + "/cancel/%s"
        self.BOOKINGS_URL = self.BASE_URL + "/bookings/"
        self.username = username
        self.password = password

    def search(self, params):
        """
        /search startpoint caller
        :param params: Product code
        :return:response as JSON

        """
        resp = requests.get(self.SEARCH_URL,
                            params=params, auth=(self.username, self.password))
        return resp.json()

    def availability(self, product_code):
        """
        /availability endpoint caller.
        :param product_code: Product code
        :return:response as JSON

        """

        availability_response = requests.get(
            self.AVAILABILITY_URL % product_code,
            auth=(
                self.username, self.password))
        if availability_response.status_code == 200:
            return availability_response.json(), \
                   availability_response.status_code
        else:
            raise ValueError('No Longer Available')

    def provision(self, product_code):
        """
        /provision endpoint caller.
        :param product_code: Product code
        :return:response as JSON

        """

        provision_response = requests.post(self.PROVISION_URL % product_code,
                                           auth=(self.username, self.password))

        if provision_response.status_code == 200:
            return provision_response.json(), provision_response.status_code
        else:
            raise ValueError('No Longer Available')

    def book(self, book_code, params):
        """
        /book endpoint request.
        :param book_code: Provision code
        :param params: params
        :return:response as JSON

        """
        book_response = requests.post(self.BOOK_URL % book_code,
                                      data=params,
                                      auth=(self.username, self.password))

        if book_response.status_code == 200:
            return book_response.json(), book_response.status_code
        else:
            raise ValueError('No Longer Available')

    def cancel(self, book_code):
        """
        /cancel cancellation policy book.
        :param book_code: Book code
        :return:response as JSON

        """

        cancel_response = requests.post(self.CANCEL_URL % book_code,
                                        auth=(self.username, self.password))

        if cancel_response.status_code == 200:
            return cancel_response.json(), cancel_response.status_code
        else:
            raise ValueError('No Longer Available')

    def bookings(self, book_code=None):
        """
        /Consumer's bookings list.
        :return: booking_list

        """

        url = self.BOOKINGS_URL

        if book_code:
            url += book_code

        bookings_response = requests.get(url,
                                         auth=(
                                             self.username, self.password))

        if bookings_response.status_code == 200:
            return bookings_response.json(), bookings_response.status_code
        else:
            print bookings_response.status_code
            raise ValueError('No Longer Available')
