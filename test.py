from unittest import TestCase, main
from booker import Booker


class Test(TestCase):
    product_code = ""
    book_code = ""

    def __init__(self, *arg, **kwargs):
        self.params = {'checkin': '2016-11-20', 'checkout': '2016-11-22',
                       'pax': '1',
                       'hotel_code': '1041b4', 'client_nationality': 'tr',
                       'currency': 'USD', 'name': '1,test,test,adult'}

        self.booker = Booker(username="caaping",
                             password="21101905")

        try:
            self.search_response = self.booker.search(params=self.params)
            self.product_code = \
                self.search_response["results"][0]["products"][0]["code"]
        except:
            raise

        super(Test, self).__init__(*arg, **kwargs)

    def test_search(self):
        self.assertGreaterEqual(self.search_response['count'], 1)
        self.assertIn('code',
                      self.search_response['results'][0]['products'][0])

    def test_search_wrong_hotel_code(self):
        new_params = {'checkin': '2016-11-20', 'checkout': '2016-11-22',
                      'pax': '1',
                      'hotel_code': '1041b4aa', 'client_nationality': 'tr',
                      'currency': 'USD', 'name': '1,test,test,adult'}

        with self.assertRaises(IndexError):
            self.search_response = self.booker.search(params=new_params)
            self.product_code = \
                self.search_response["results"][0]["products"][0]["code"]

    def test_search_wrong_authentication(self):
        booker = Booker(username="a",
                        password="b")

        with self.assertRaises(KeyError):
            self.search_response = booker.search(params=self.params)
            self.product_code = \
                self.search_response["results"][0]["products"][0]["code"]

    def test_availability(self):
        availability_response = self.booker.availability(
            product_code=self.product_code)
        self.assertIsInstance(availability_response[0], dict)
        self.assertEquals(200, availability_response[1])

    def test_availability_fail_state(self):
        with self.assertRaises(ValueError):
            self.booker.availability(product_code="test")

    def test_provision(self):

        try:
            self.booker.provision(product_code=self.product_code)
        except Exception as e:
            print type(e)
            raise

    def test_provision_fail_state(self):
        with self.assertRaises(ValueError):
            self.booker.provision(product_code="test")

    def test_book(self):
        self.book_code = self.booker.provision(self.product_code)[0]["code"]
        book_response = self.booker.book(self.book_code, params=self.params)
        self.assertIn('code', book_response[0])
        self.assertEqual(200, book_response[1])

    def test_book_fail_state(self):
        with self.assertRaises(ValueError):
            self.book_code = self.booker.provision("test")[0]["code"]
            book_response = self.booker.book(book_code=self.book_code,
                                             params=self.params)
            self.assertIsNone(book_response.json())

    def test_cancel(self):
        self.book_code = self.booker.provision(self.product_code)[0]["code"]
        book_response = self.booker.book(self.book_code, params=self.params)
        cancel_response = self.booker.cancel(book_response[0]["code"])
        self.assertIsInstance(cancel_response[0], dict)
        self.assertIn('code', cancel_response[0])
        self.assertEquals(200, cancel_response[1])

    def test_cancel_wrong_book_code(self):
        with self.assertRaises(ValueError):
            book_response = self.booker.book("test", params=self.params)
            self.booker.cancel(book_response[0]["code"])

    def test_bookings(self):
        bookings_response = self.booker.bookings()
        self.assertIsInstance(bookings_response[0], list)

        provision, _ = self.booker.provision(self.product_code)
        book, _ = self.booker.book(provision['code'], params=self.params)
        bookings, _ = self.booker.bookings(book_code=book['code'])
        self.assertEquals(bookings['status'], 'succeeded')

    def test_bookings_fail_state(self):
        with self.assertRaises(ValueError):
            self.booker.bookings(book_code="test")


if __name__ == '__main__':
    main()
